import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //Color color = Theme.of(context).primaryColor;
    Widget resumeSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                padding: const EdgeInsets.only(
                  bottom: 8,
                ),
                child: Text(
                  'ประวัติส่วนตัว',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 35),
                ),
              ),
              Text(
                  'ชื่อ: เอกรัตน์ เกษรสมบัติ' +
                      '\n' +
                      'ชื่อเล่น: เต๋า    อายุ: 21 ปี' +
                      '\n' +
                      'เกิดวันที่: 28 สิงหาคม 2542' +
                      '\n' +
                      'กำลังศึกษาที่:' +
                      '\n' +
                      'มหาวิทยาลัยบูรพา วิทยาเขตบางแสน' +
                      '\n' +
                      'คณะวิทยาการสารสนเทศ' +
                      '\n' +
                      'สาขาวิทยาการคอมพิวเตอร์ (Computer Science)' +
                      '\n' +
                      'GPA: 2.56 (6 ภาคเรียน)' +
                      '\n' +
                      'งานอดิเรก: เล่นเกม ฟังเพลง วาดรูป',
                  style: TextStyle(fontSize: 20, color: Colors.amber[500])),
            ],
          )),
        ],
      ),
    );
    Widget myskillSection = Container(
      child: Row(
        children: [
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                padding: const EdgeInsets.only(
                  bottom: 8,
                ),
                child: Text(
                  'My Skills',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 35),
                ),
              ),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Image.asset(
                      'image/css.png',
                      width: 80,
                      height: 90,
                    ),
                    Image.asset(
                      'image/html.png',
                      width: 80,
                      height: 90,
                    ),
                    Image.asset(
                      'image/JavaScript.png',
                      width: 140,
                      height: 150,
                    ),
                    Image.asset(
                      'image/java.png',
                      width: 80,
                      height: 90,
                    ),
                    Image.asset(
                      'image/vue.png',
                      width: 80,
                      height: 90,
                    ),
                    Image.asset(
                      'image/sc1.png',
                      width: 140,
                      height: 150,
                    ),
                  ],
                ),
              ),
            ],
          )),
        ],
      ),
    );
    Widget myeducationSection = Container(
      child: Row(
        children: [
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                padding: const EdgeInsets.only(
                  bottom: 8,
                ),
                child: Text(
                  'My Education',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 35),
                ),
              ),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      child: Column(
                        children: [
                          Image.asset(
                            'image/sch1.jpg',
                            width: 80,
                            height: 90,
                          ),
                          Text(
                            'ชั้นอนุบาลและ' +
                                '\n' +
                                'ชั้นประถมศึกษา' +
                                '\n' +
                                'โรงเรียนวัดนาคนิมิตร',
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                    ),
                    Container(
                      child: Column(
                        children: [
                          Image.asset(
                            'image/sch2.png',
                            width: 80,
                            height: 90,
                          ),
                          Text(
                            'ชั้นมัธยมศึกษาตอนต้น' + '\n' + 'โรงเรียนวัดยายร่ม',
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                    ),
                    Container(
                      child: Column(
                        children: [
                          Image.asset(
                            'image/sch3.jpg',
                            width: 80,
                            height: 90,
                          ),
                          Text(
                            'ชั้นมัธยมศึกษาตอนปลาย' +
                                '\n' +
                                'โรงเรียนวัดราชโอรส',
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                    ),
                    Container(
                      child: Column(
                        children: [
                          Image.asset(
                            'image/buu.png',
                            width: 80,
                            height: 90,
                          ),
                          Text(
                            'ระดับปริญญาตรี' +
                                '\n' +
                                '(กำลังศึกษา)' +
                                '\n' +
                                'มหาวิทยาลัยบูรพา',
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          )),
        ],
      ),
    );
    return MaterialApp(
      title: 'Welcome to My Resume',
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.amber[600],
          leading: Icon(Icons.sentiment_very_satisfied,
              size: 40, color: Colors.black),
          title: const Text('My Resume',
              style:
                  TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
        ),
        body: ListView(children: [
          Image.asset(
            'image/myprofile.jpg',
            width: 550,
            height: 600,
          ),
          resumeSection,
          myskillSection,
          myeducationSection
        ]),
        bottomNavigationBar: BottomAppBar(
          color: Colors.amber[700],
          shape: const CircularNotchedRectangle(),
          child: Container(
              height: 50,
              child: Icon(
                Icons.mode_edit_outline,
                size: 20,
              )),
        ),
      ),
    );
  }
}
